#  TP1 Exercice 9(a&b)

## Objectif
L'objectif de cet exercice est d'importer les classes contenues dans le package Package_Calculator, grâce à test Pypi, permettant ainsi d'importer les modules automatiquement, sans modifier le PYTHONPATH à chaque fois qu'on souhait utiliser les fonctions de Calculator.py.

## Méthode
On récupère la même organisation que l'exercice précédent.   
On créé un fichier python setup.py permettant de référencer le nom du dossier et le nom du package qu'il représente.
On créer un compte sur le site Test.Pypi.org, puis on créér un token  
On upload ensuite le token et on installe le package.  

## Installation du package
Dans un premier temps, il faut installer des modules pour l'importation et l'upload du package:

    python3 -m pip install --user --upgrade setuptools wheel

    python3 setup.py sdist bdist_wheel
    
puis une fois le token créé sur test.Pypi on peut entrer les commandes permettant l'upload :

    python3 -m pip install --user --upgrade twine
    
    python3 -m twine upload --repository pypi dist/*
    
Il faut à présent installer le package dans un environement virtuel :

    python3 -m venv tuto_venv
    source ./venv/bin/activate
    
    pip install Package-Calculator-Complex #( commande donnée sur le site test.pytp)
    


## Ressources
https://packaging.python.org/tutorials/packaging-projects/#packaging-your-project
https://gitlab.com/PaulBx/testpipy

